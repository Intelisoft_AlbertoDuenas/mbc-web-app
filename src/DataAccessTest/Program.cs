﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessTest
{
    class Program
    {
        static void Main(string[] args)
        {
            UserDao UDao = new UserDao();

            User user = new User();
            user.FirstName = "2Alberto";
            user.LastName = "2Duenas";
            UDao.SaveUser(user);

            List<User> Users = UDao.List();
            foreach (User iU in Users)
            {
                Console.WriteLine(iU.FirstName + " " + iU.LastName + Environment.NewLine);
            }
            Console.Read();
        }
    }
}
