﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebExample.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        private readonly UserDao Users = new UserDao();

        public ActionResult Index()
        {
            ViewBag.Amparo = "100";
            var People = Users.List();
            return View(People);
        }
    }
}