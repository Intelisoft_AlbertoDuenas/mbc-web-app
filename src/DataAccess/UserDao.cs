﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UserDao
    {
        private readonly MBCWebAppEntities mBC = new MBCWebAppEntities();

        public void SaveUser(User user)
        {
            mBC.Users.Add(user);
            mBC.SaveChanges();
        }

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        public List<User> List()
        { 
            return mBC.Users.ToList();
        }
    }
}
